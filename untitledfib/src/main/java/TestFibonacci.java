import org.junit.Assert;
import org.junit.Test;

public class TestFibonacci {

    @Test
    public void testFib() {
        int[] myFib = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946};
        for (int i = 0; i < myFib.length; i++) {
            Assert.assertEquals(myFib[i], Fibonacci.fib(i));
        }
    }

    @Test
    public void testFibPerformance() {
        Assert.assertEquals(190392490709135L, Fibonacci.fib(70));
    }
}
