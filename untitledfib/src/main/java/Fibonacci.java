public class Fibonacci {

    public static long fib(int i) {
        return fib(i, 0, 1);
    }

    public static long fib(int n, long f1, long f2) {
        if (n < 2)
            return n;
        if (n == 2)
            return f1 + f2;
        return fib(n - 1, f2, f2 + f1);
    }
}
